/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoohashing;

import static com.mycompany.cuckoohashing.Cuckoo.cuckoo;

/**
 *
 * @author kitti
 */
public class MainCuckoo {

    public static void main(String[] args) {
        int keys_1[] = {20, 50, 53, 75, 100,
            67, 105, 3, 36, 39};

        int n = keys_1.length;

        cuckoo(keys_1, n);

    }
}
